# Git Commands Project

This repository is to test your knowledge on Basic Git Workflows.  

**Please perform the following**:
* Fork this repo.
  * Setting up Repository Mirroring is the only way to ensure you keep your fork synced. 
* Fix the Error in this ReadMe.
* Submit a Pull Request to the original Development branch.

